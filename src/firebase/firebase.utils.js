import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyCeKy9uYFXN5r1aJppd1M0ZYNqlbjEpoK0",
  authDomain: "ecomercewebsiterm.firebaseapp.com",
  projectId: "ecomercewebsiterm",
  storageBucket: "ecomercewebsiterm.appspot.com",
  messagingSenderId: "294623494346",
  appId: "1:294623494346:web:5cef14983badd7ce927433",
  measurementId: "G-G9E1083KJZ"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
